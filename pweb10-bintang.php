<!DOCTYPE html>
<html>
<head>
    <title>Tugas Pweb 10 - Bintang</title>
</head>
<body>
<pre>
<?php
$tinggi = 5; // You can change this value to prompt user input using a form
for ($baris = 1; $baris <= $tinggi; $baris++) {
    // Buat sejumlah spasi
    for ($i = 1; $i < $tinggi - $baris+1; $i++) {
        echo " "; // Karakter spasi
    }
    // Tampilkan *
    for ($j = 1; $j < 2 * $baris; $j++) {
        echo "*";
    }
    // Pindah baris
    echo "\n";
}
?>
</pre>
</body>
</html>
