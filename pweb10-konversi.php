<!DOCTYPE html>
<html>
<head>
    <title>Tugas Pweb 10 - 1</title>
</head>
<body>
    <h2>Konversi Nilai Angka ke Nilai Huruf</h2>
    <form method="post" action="">
        <label for="nilai">Masukkan Nilai Angka:</label>
        <input type="number" id="nilai" name="nilai" required>
        <input type="submit" value="Konversi">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nilai = $_POST['nilai'];
        
        if ($nilai >= 80 && $nilai <= 100) {
            $nilaiHuruf = "A";
        } elseif ($nilai >= 76.25 && $nilai < 79.99) {
            $nilaiHuruf = "A-";
        } elseif ($nilai >= 68.75 && $nilai < 76.24) {
            $nilaiHuruf = "B+";
        } elseif ($nilai >= 65 && $nilai < 68.74) {
            $nilaiHuruf = "B";
        } elseif ($nilai >= 62.5 && $nilai < 64.99) {
            $nilaiHuruf = "B-";
        } elseif ($nilai >= 57.5 && $nilai < 62.49) {
            $nilaiHuruf = "C+";
        } elseif ($nilai >= 55 && $nilai < 57.49) {
            $nilaiHuruf = "C";
        } elseif ($nilai >= 51.25 && $nilai < 54.99) {
            $nilaiHuruf = "C-";
        } elseif ($nilai >= 43.75 && $nilai < 51.24) {
            $nilaiHuruf = "D+";
        } elseif ($nilai >= 40 && $nilai < 43.74) {
            $nilaiHuruf = "D";
        } elseif ($nilai >= 0 && $nilai < 39.99) {
            $nilaiHuruf = "E";
        } else {
            $nilaiHuruf = "Nilai tidak dapat dikonversi";
        }

        echo "<h3>Hasil Konversi:</h3>";
        echo "<p>Nilai Angka: $nilai</p>";
        echo "<p>Nilai Huruf: $nilaiHuruf</p>";
    }
    ?>
</body>
</html>
